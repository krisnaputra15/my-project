<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\login;
use Validator;
use Session;
use DB;

class loginController extends Controller
{
    public function login(){
        return view('login');
    }
    public function register(){
        return view('register');
    }
    public function reg_process(request $r){
        $validator = Validator::make($r->all(), [
            "nama" => "required",
            "alamat" => "required",
            "email" => "required",
            "username" => "required",
            "password" => "required|min:8",
        ]);
        if($validator->fails()){
            Session::flash('alert_pesan','Unvalid identity');
            return redirect('/registernya');
        }
        else{
            $create = login::create([
                "nama" => $r->nama,
                "alamat" => $r->alamat,
                "email" => $r->email,
                "username" => $r->username,
                "pass" => md5($r->password)
            ]);
            $search = DB::table('user')->where('email','=',$r->email,'or','username','=',$r->username)
                      ->select('user.*')
                      ->count();
            if($seacrh > 0){
                Session::flash('alert_pesan2','Username atau email sudah terdaftar');
                return redirect('/registernya');
            }
            else{
                if($create){
                    Session::flash('alert_gud','berhasil register');
                    return redirect('/loginnya');
                }
                else{
                    Session::flash('alert_bad','gagal register');
                    return redirect('/registernya');
                }
            }
        }
    }
    public function log_process(request $r){
        $login = DB::table('user')->where('user.username','=',$r->username)
                 ->where('user.pass','=',md5($r->password))
                 ->select('user.*')
                 ->first();
        if($login){
            Session::put('id',$login->id);
            Session::put('nama',$login->nama);
            Session::put('alamat',$login->alamat);
            Session::put('email',$login->email);
            Session::put('username',$login->username);
            Session::put('password',md5($login->pass));
            
            Session::flash('alert_gud','berhasil login');
            return redirect('/afterlog');

        }
        else{
            Session::flash('alert_bad','gagal login');
            return redirect('/loginnya');
        }
    }
    public function afterlog(){
        return view('afterlog');
    }
    public function logout(){
        Session::flush();
        Session::flash('alert_logout','berhasil logout');
        return redirect('/loginnya');
    }
}
