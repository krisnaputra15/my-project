<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class login extends Model
{
    protected $table = "user";
    protected $fillable = [
        'nama', 'alamat', 'email', 'username', 'pass',
    ];
    public $timestamps = false;
}
