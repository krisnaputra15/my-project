<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}"/>
</head>
<body>
<div class="card-body">
@if(Session::has('alert_gud'))
        <div class="alert alert-success">
            {{Session('alert_gud')}}
        </div>
@endif
        <h1>Data user</h1>
        <p>id : {{Session('id')}}</p>
        <p>nama : {{Session('nama')}}</p>
        <p>alamat : {{Session('alamat')}}</p>
        <p>email : {{Session('email')}}</p>
        <p>username : {{Session('username')}}</p>
        <p>password : {{Session('password')}}</p>
        <a href="{{url('/logout')}}">logout</a>
</div>
</body>
</html>