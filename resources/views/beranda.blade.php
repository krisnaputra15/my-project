@extends('template.all')
@section('judul')
Beranda
@endsection

@section('slider')
<div class="container">

		<div class="bd-example">
			<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
					<li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                </ol>
                <center>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img src="img/dashboard-slider1.png" class="d-block w-100" alt="...">
						<div class="carousel-caption d-none d-md-block">
							<h5>Gambar Slide Yang Pertama</h5>
							<p>Gambar pemandangan sungai.</p>
						</div>
					</div>
					<div class="carousel-item">
						<img src="image2.jpg" class="d-block w-100" alt="...">
						<div class="carousel-caption d-none d-md-block">
							<h5>Gambar Slide Yang Kedua</h5>
							<p>Gambar pemandangan sawah di desa.</p>
						</div>
					</div>
					<div class="carousel-item">
						<img src="image3.jpg" class="d-block w-100" alt="...">
						<div class="carousel-caption d-none d-md-block">
							<h5>Gambar Slide Yang Ketiga</h5>
							<p>Gambar pemandangan taman belakang rumah.</p>
						</div>
					</div>
                </div>
                </center>
				<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>

	</div>

	<script src="assets/js/jquery.js"></script> 
	<script src="assets/js/popper.js"></script> 
	<script src="assets/js/bootstrap.js"></script
@endsection