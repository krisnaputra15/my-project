<!DOCTYPE html>

<html lang="id" style="transform: none;">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-param" content="_csrf">
<meta name="csrf-token" content="PDxlUUSBGu19sqJH148fCwZfuHJRhO8S9OgA7GjvvXx7X1EBDNhbpBKHwSqE4C15SW7KRTDehCahvlWKDIrSNA==">
    <title>@yield('judul')</title>
    <link href="rsud-kanjuruhan.malangkab.go.id_files/css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/bootstrap.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/style.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/dark.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/font-icons.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/animate.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/magnific-popup.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/bs-switches.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/responsive.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/colors.php" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/fonts.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/fonts(1).css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/news.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/swiper.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/construction.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/bootstrap.min.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/pe-icon-7-stroke.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/font-awesome.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/settings.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/layers.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/navigation.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/revolution.addon.revealer.css" rel="stylesheet">
<link href="rsud-kanjuruhan.malangkab.go.id_files/revolution.addon.revealer.preloaders.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<style type="text/css">
    	.img-valign {
		 	vertical-align: middle;
		  	margin-bottom: 0.75em;
		  	max-width: 100px;
		}

		.text1 {
		  	font-size: 44px;
		}

		.text2 {
		  	font-size: 18px;
		}
    </style>
<style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>
<style id="scw-sticky-sidebar-stylesheet-TSS">.scwStickySidebar:after {content: ""; display: table; clear: both;}</style>
<script charset="utf-8" src="./rsud-kanjuruhan.malangkab.go.id_files/button.e24f3bcdec527b80b9c80e88b62047c3.js"></script>
<script charset="utf-8" src="./rsud-kanjuruhan.malangkab.go.id_files/moment_timeline_tweet.2e5232162202896d50461b242819754e.js"></script>
<script charset="utf-8" src="./rsud-kanjuruhan.malangkab.go.id_files/timeline.610564c46865d0bb1eccdd42c0dc6ea7.js"></script>
</head>

<body class="stretched no-transition device-x1" style="transform: none">
    <!-- Document Wrapper
	============================================= -->
    <div id="wrapper">

        <!-- Top Bar
		============================================= -->
        <div id="top-bar">
            <div id="container clearfix">
                <div id="col_half nobottomargin clearfix">

                <!-- Top Social
					============================================= -->
                    <div id="top-social">
                        <ul>
                            <li><a href="https://www.facebook.com/rsudkanjuruhankabupatenmalang" class="si-facebook" data-hover-width="112.6875" style="width: 40px;"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
                            <li><a href="https://twitter.com/rsudkanjuruhan" class="si-twitter" data-hover-width="93.6562" style="width: 40px;"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
                            <li><a href="https://www.instagram.com/rsudkanjuruhan" class="si-instagram" data-hover-width="114.1406" style="width: 40px;"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
                            <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/" class="si-youtube" data-hover-width="103.0312" style="width: 40px;"><span class="ts-icon"><i class="icon-youtube"></i></span><span class="ts-text">Youtube</span></a></li>
                        </ul>
                    </div> 
                <!-- Top social end -->
                </div>

                <div class="col_half fright col_last celarfix nobottomargin">

                <!-- Top Links
					============================================= -->
                    <div class="top-links">
                        <ul>
                            <li><a href="https://sirup.lkpp.go.id/">SIRUP</a></li>
                            <li><a href="https://lapor.go.id/">LAPOR</a></li>
                        </ul>
                    </div> <!-- Top links end -->
                </div>
            </div>
        </div> <!-- top bar end -->

    <!-- Header
	============================================= -->
	<header id="header" class="sticky-style-2">

        <div class="container clearfix">

        <!-- Logo
            ============================================= -->
            <a href="http://rsud-kanjuruhan.malangkab.go.id/pd/page/index" class="img-valign" data-dark-logo="images/logo.png"><img src="./rsud-kanjuruhan.malangkab.go.id_files/logo_kabmalang.png" alt=""><span class="text2 text-primary" style="max-width: 350px; word-wrap: break-word; display: table-cell;;"><strong style="color: #90d26d">RSUD Kanjuruhan</strong></span></a>
    
        <!-- #logo end -->

            <ul class="header-extras">
                <li>
                    <i class="i-plain icon-call nomargin"></i>
                     <div class="he-text pull left">
                        Telepon
                        <span>0341395041</span>
                    </div>
                </li>
                <li>
                    <i class="i-plain icon-line2-envelope nomargin"></i>
                    <div class="he-text">
                        Email 
                        <span>rsud-kanjuruhan@malangkab.go.id</span>
                    </div>
                </li>
                <!-- <li>
                    <i class="i-plain icon-line-clock nomargin"></i>
                    <div class="he-text">
                        Jam Kerja
                        <span>Senin - Jum'at, 08:00 - 17:00</span>
                    </div>
                </li> -->
            </ul>

        </div>

        <div id="header-wrap">

        <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu" class="with-arrows style-2 center">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <!-- <ul>
                    <li><a href="/pd/default"><div>Beranda</div></a></li>
                    <li class=""><a href="#"><div>Profil</div></a>
                        <ul>
                                                                <li><a href="/pd/profil/go?id=7"><div>Tujuan dan Sasaran</div></a></li>
                                                                <li><a href="/pd/profil/go?id=1225"><div>Sumber Daya Manusia</div></a></li>
                                                                <li><a href="/pd/profil/go?id=1226"><div>Prestasi Dinas Kominfo</div></a></li>
                                                                <li><a href="/pd/profil/go?id=1228"><div>Tentang Kami</div></a></li>
                                                                <li><a href="/pd/profil/go?id=3312"><div>Tugas Pokok Dan Fungsi</div></a></li>
                                                                <li><a href="/pd/profil/go?id=5710"><div>Struktur Organisasi</div></a></li>
                                                            </ul>
                    </li>
                    <li class=""><a href="#"><div>Program</div></a>
                        <ul>
                                                                <li><a href="/pd/program/go?id=9"><div>Kelompok Informasi Masyarakat (KIM)</div></a></li>
                                                                <li><a href="/pd/program/go?id=10"><div>Arsitektur e-Goverment</div></a></li>
                                                                <li><a href="/pd/program/go?id=11"><div>Hasil Pengembangan</div></a></li>
                                                                <li><a href="/pd/program/go?id=53"><div>Pelatihan Website Desa</div></a></li>
                                                                <li><a href="/pd/program/go?id=1231"><div>Kelompok Informasi Masyarakat</div></a></li>
                                                                <li><a href="/pd/program/go?id=1232"><div>Arsitektur e-Government</div></a></li>
                                                                <li><a href="/pd/program/go?id=1233"><div>Hasil Pembangunan</div></a></li>
                                                                <li><a href="/pd/program/go?id=4325"><div>Pemetaan Infrastruktur Telekomunikasi</div></a></li>
                                                            </ul>
                    </li>
                    <li class=""><a href="/pd/berita"><div>Berita</div></a></li>
                    <li class=""><a href="/pd/galeri"><div>Galeri</div></a></li>
                    <li class=""><a href="/pd/download"><div>Download</div></a></li>
                    <li class=""><a href="/pd/interaksi"><div>Interaksi</div></a></li>
                </ul> -->

                <ul id="w0" class="nav sf-js-enabled" style="touch-action: pan-y;"><li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/index">Beranda</a></li>
                    <li class="dropdown sub-menu"><a class="dropdown-toggle sf-with-ul" href="http://rsud-kanjuruhan.malangkab.go.id/pd/#" data-toggle="dropdown">Profil <span class="caret"></span></a><ul id="w1" class="dropdown-menu" style="display: none;"><li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=visi-dan-misi-5" tabindex="-1">Visi Misi</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=rsud-kanjuruhan-sejarah-3" tabindex="-1">SEJARAH</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=prestasi-rsud-kanjuruhan" tabindex="-1">PRESTASI</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=rsud-kanjuruhan-struktur-organisasi-3" tabindex="-1">STRUKTUR ORGANISASI</a></li></ul></li>
                    <li class="dropdown sub-menu"><a class="dropdown-toggle sf-with-ul" href="http://rsud-kanjuruhan.malangkab.go.id/pd/#" data-toggle="dropdown">Program <span class="caret"></span></a><ul id="w2" class="dropdown-menu" style="display: none;"><li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=indikator-mutu" tabindex="-1">INDIKATOR MUTU NASIONAL 2019</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=indikator-mutu-tahun-2019-lanjutan" tabindex="-1">INDIKATOR MUTU NASIONAL 2019 lanjutan</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=rsud-kanjuruhan-surver-kepuasan-masyarakat" tabindex="-1">SURVEY KEPUASAN MASYARAKAT</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=rsud-kanjuruhan-standar-pelayanan-rsud" tabindex="-1">STANDAR PELAYANAN</a></li></ul></li>
                    <li class="dropdown sub-menu"><a class="dropdown-toggle sf-with-ul" href="http://rsud-kanjuruhan.malangkab.go.id/pd/#" data-toggle="dropdown">Pelayanan <span class="caret"></span></a><ul id="w3" class="dropdown-menu" style="display: none;"><li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=rsud-kanjuruhan-fasilitas-rawat-jalan" tabindex="-1">FASILITAS RAWAT JALAN/ POLIKLINIK</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=rsud-kanjuruhan-fasilitas-penunjang" tabindex="-1">FASILITAS PENUNJANG</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=rsud-kanjuruhan-alur-penanganan-pengaduan" tabindex="-1">ALUR PENANGANAN PENGADUAN</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=rsud-kanjuruhan-alur-pendaftaran-pasien" tabindex="-1">ALUR PENDAFTARAN PASIEN</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=rsud-kanjuruhan-jadwal-dokter" tabindex="-1">DAFTAR DOKTER</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=rsud-kanjuruhan-fasilitas-pelayanan" tabindex="-1">FASILITAS PELAYANAN</a></li></ul></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/berita">Berita</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/galeri">Galeri</a></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/interaksi">Interaksi</a></li>
                    <li class="dropdown sub-menu"><a class="dropdown-toggle sf-with-ul" href="http://rsud-kanjuruhan.malangkab.go.id/pd/#" data-toggle="dropdown">DOWNLOAD <span class="caret"></span></a><ul id="w4" class="dropdown-menu" style="display: none;"><li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/slug?title=rsud-kanjuruhan-sk-direktur-ttg-standar-pelayanan" tabindex="-1">STANDAR PELAYANAN</a></li></ul></li>
                    <li><a href="http://rsud-kanjuruhan.malangkab.go.id/pd/sakip">SAKIP</a></li></ul>
                <!-- Top Search
                    ============================================ -->
                    <div id="top-search">
                        <a href="http://rsud-kanjuruhan.malangkab.go.id/pd/#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                            <form id="w5" action="http://rsud-kanjuruhan.malangkab.go.id/pd/page/berita" method="get">
    
                            <div class="form-group field-globalartikelsearch-global_search">
                                <input type="text" id="globalartikelsearch-global_search" class="form-control" name="GlobalArtikelSearch[global_search]" placeholder="Cari berita..">

                                <div class="help-block">
                                </div>
                            </div>
                            </form>						
                    </div><!-- #top-search end -->

                </div>

            </nav><!-- #primary-menu end -->

        </div>

    </header><!-- #header end -->
    </div>


    <!-- Slider
		============================================= -->
        @yield('slider')
    <!-- Slider end -->

    <!-- Content
        ============================================= -->
        @yield('konten')
    <!-- Content end -->

</body>

</html>
