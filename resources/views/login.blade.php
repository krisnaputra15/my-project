<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}"/>
</head>
<body>
<div class="card-body">
@if(Session::has('alert_gud'))
        <div class="alert alert-success">
            {{Session('alert_gud')}}
        </div>
@endif

@if(Session::has('alert_bad'))
        <div class="alert alert-danger">
            {{Session('alert_bad')}}
        </div>
@endif

@if(Session::has('alert_logout'))
        <div class="alert alert-success">
            {{Session('alert_logout')}}
        </div>
@endif
<form method="POST" action="{{url('log-proses')}}">
		@csrf
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-user"></i></span>
						</div>
						<input type="text" class="form-control" name="username" placeholder="username/email">
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-key"></i></span>
						</div>
						<input type="password" class="form-control" name="password" placeholder="password">
					</div>
				
					<div class="form-group">
						<input type="submit" value="Login" class="btn btn-danger" style="margin-left: 20%;">
					</div>
				</form>
</div>
</body>
</html>