<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('loginnya','loginController@login');
Route::get('registernya','loginController@register');
Route::post('reg-proses','loginController@reg_process');
Route::post('log-proses','loginController@log_process');
Route::get('afterlog','loginController@afterlog');
Route::get('logout','loginController@logout');
Route::get('beranda','viewController@beranda');

Auth::routes();


